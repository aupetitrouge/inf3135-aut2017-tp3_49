#include "gameOver.h"
#include "spritesheet.h"
#include <SDL2/SDL.h>

struct GameOver *GameOver_initialize(SDL_Renderer *renderer, int win)
{
	struct GameOver *gameOver;
	gameOver = (struct GameOver*)malloc(sizeof(struct GameOver));
	gameOver->renderer = renderer;
	gameOver->choice = GAMEOVER_NONE;
	if (win)
		gameOver->image = Spritesheet_create(VICTORY_FILENAME, 1, 1, 1, renderer);
	else
		gameOver->image = Spritesheet_create(DEFEAT_FILENAME, 1, 1, 1, renderer);
	return gameOver;
}

void GameOver_run(struct GameOver *gameOver)
{
	SDL_Event e;
	while (gameOver->choice == GAMEOVER_NONE) {
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				gameOver->choice = GAMEOVER_QUIT;
			} else if (e.type == SDL_KEYDOWN) {
				gameOver->choice = GAMEOVER_CONTINUE;
			}
		}
		SDL_SetRenderDrawColor(gameOver->renderer, 0x00, 0x00, 0x00, 0x00 );
		SDL_RenderClear(gameOver->renderer);
		Spritesheet_render(gameOver->image, GAMEOVER_X, GAMEOVER_Y, 0);
		SDL_RenderPresent(gameOver->renderer);
	}
}

void GameOver_delete(struct GameOver *gameOver)
{
	if (gameOver != NULL) {
		Spritesheet_delete(gameOver->image);
		free(gameOver);
	}
}
